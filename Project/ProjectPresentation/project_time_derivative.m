function [ func_dot ] = project_time_derivative( func )
global linkParams linkParamsC TSolve
global z0 q qd qdd
global qdes qddes qdddes qei
global th thd thdd
global I
global B tau taud ode45_eq ode45_u
global KP KI KD
syms  th1des d2des d3des d4des th5des
syms  th1ddes d2ddes d3ddes d4ddes th5ddes
syms  th1dddes d2dddes d3dddes d4dddes th5dddes
syms  g
syms  th1 d2 d3 d4 th5
syms  th1d d2d d3d d4d th5d
syms  th1dd d2dd d3dd d4dd th5dd
syms  th1ei d2ei d3ei d4ei th5ei
syms  m1 m2 m3 m4 m5
syms  Ix1 Ix2 Ix3 Ix4 Ix5
syms  Iy1 Iy2 Iy3 Iy4 Iy5
syms  Iz1 Iz2 Iz3 Iz4 Iz5
syms  nx1 ny1 nz1
syms  ox1 oy1 oz1
syms  ax1 ay1 az1
syms  px1 py1 pz1
syms  B1 B2 B3 B4 B5
syms  tau1 tau2 tau3 tau4 tau5
syms  tau1d tau2d tau3d tau4d tau5d
syms  KP1 KP2 KP3 KP4 KP5
syms  KI1 KI2 KI3 KI4 KI5
syms  KD1 KD2 KD3 KD4 KD5
syms  l4 l5

% Given a symbolic value, I derive the time derivative for known
% derivatives
func_dot = sym(0);
for i=1:5
    func_dot = func_dot + diff(func,qei(i))*(q(i)-qdes(i));
    func_dot = func_dot + diff(func,q(i))*qd(i);
    func_dot = func_dot + diff(func,qd(i))*qdd(i);
    func_dot = func_dot + diff(func,tau(i))*taud(i);
end
func_dot = simplify(func_dot);
end


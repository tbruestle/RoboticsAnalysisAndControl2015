% project_setGlobals.m
%
% Sets the global parameters for the project
%
function setgb = project_setGlobals()
%HW7_SETGLOBALS Sets globals for HW7
global linkParams linkParamsC TSolve
global z0 q qd qdd qei
global qdes qddes qdddes
global qstart qdstart qddstart qeistart
global th thd thdd
global I tspan x0
global B tau taud ode45_eq ode45_u
global KP KI KD con_vals
global m
global Xd Xdd

syms  th1des d2des d3des d4des th5des
syms  th1ddes d2ddes d3ddes d4ddes th5ddes
syms  th1dddes d2dddes d3dddes d4dddes th5dddes

syms  th1start d2start d3start d4start th5start
syms  th1dstart d2dstart d3dstart d4dstart th5dstart
syms  th1ddstart d2ddstart d3ddstart d4ddstart th5ddstart
syms  th1eistart d2eistart d3eistart d4eistart th5eistart

syms  g
syms  th1 d2 d3 d4 th5
syms  th1d d2d d3d d4d th5d
syms  th1dd d2dd d3dd d4dd th5dd
syms  th1ei d2ei d3ei d4ei th5ei
syms  m1 m2 m3 m4 m5
syms  Ix1 Ix2 Ix3 Ix4 Ix5
syms  Iy1 Iy2 Iy3 Iy4 Iy5
syms  Iz1 Iz2 Iz3 Iz4 Iz5
syms  nx1 ny1 nz1
syms  ox1 oy1 oz1
syms  ax1 ay1 az1
syms  px1 py1 pz1
syms  B1 B2 B3 B4 B5
syms  tau1 tau2 tau3 tau4 tau5
syms  tau1d tau2d tau3d tau4d tau5d
syms  KP1 KP2 KP3 KP4 KP5
syms  KI1 KI2 KI3 KI4 KI5
syms  KD1 KD2 KD3 KD4 KD5
syms  l4 l5
syms  th1err d2err d3err d4err th5err
syms  Xv1d Xv2d Xv3d 
syms  Xw1d Xw2d Xw3d
syms  Xv1dd Xv2dd Xv3dd 
syms  Xw1dd Xw2dd Xw3dd 

Xd = [ Xv1d;Xv2d;Xv3d;Xw1d;Xw2d;Xw3d ];
Xdd = [ Xv1dd;Xv2dd;Xv3dd;Xw1dd;Xw2dd;Xw3dd ];
linkParams = [ ...
    sym('th1')      0           0	sym('pi/2');	...
    sym('pi/2')     sym('d2')	0	sym('-pi/2');	...
    sym('pi/2')     sym('d3')	0	sym('pi/2');	...
    sym('pi/2')     sym('d4')	0	sym('-pi/2');	...
    sym('th5')      0           0	0               ...
    ];
linkParamsC = [ ...
    sym('th1')      0           0	sym('pi/2');	...
    sym('pi/2')     sym('d2')	0	sym('-pi/2');	...
    sym('pi/2')     sym('d3')	0	sym('pi/2');	...
    sym('pi/2')     sym('l4')	0	sym('-pi/2');	...
    sym('th5')      sym('l5')   0	0               ...
    ];

TSolve = [  sym('nx1')  sym('ox1')  sym('ax1')  sym('px1'); ...  
            sym('ny1')  sym('oy1')  sym('ay1')  sym('py1'); ...  
            sym('nz1')  sym('oz1')  sym('az1')  sym('pz1'); ...
            0           0           0           1       ...    
            ];
z0 = [0;0;1];

q = [ sym('th1');sym('d2');sym('d3');sym('d4');sym('th5') ];
qd = [ sym('th1d');sym('d2d');sym('d3d');sym('d4d');sym('th5d') ];
qdd = [ sym('th1dd');sym('d2dd');sym('d3dd');sym('d4dd');sym('th5dd') ];
qei = [ sym('th1ei');sym('d2ei');sym('d3ei');sym('d4ei');sym('th5ei') ];

qstart = [ sym('th1start');sym('d2start');sym('d3start');sym('d4start');sym('th5start') ];
qdstart = [ sym('th1dstart');sym('d2dstart');sym('d3dstart');sym('d4dstart');sym('th5dstart') ];
qddstart = [ sym('th1ddstart');sym('d2ddstart');sym('d3ddstart');sym('d4ddstart');sym('th5ddstart') ];
qeistart = [ sym('th1eistart');sym('d2eistart');sym('d3eistart');sym('d4eistart');sym('th5eistart') ];

qdes = [ sym('th1des');sym('d2des');sym('d3des');sym('d4des');sym('th5des') ];
qddes = [ sym('th1ddes');sym('d2ddes');sym('d3ddes');sym('d4ddes');sym('th5ddes') ];
qdddes = [ sym('th1dddes');sym('d2dddes');sym('d3dddes');sym('d4dddes');sym('th5dddes') ];

KP = [ sym('KP1'); sym('KP2'); sym('KP3'); sym('KP4'); sym('KP5') ];
KI = [ sym('KI1'); sym('KI2'); sym('KI3'); sym('KI4'); sym('KI5') ];
KD = [ sym('KD1'); sym('KD2'); sym('KD3'); sym('KD4'); sym('KD5') ];

th = [ sym('th1');0;0;0;sym('th5') ];
thd = [ sym('th1d');0;0;0;sym('th5d') ];
thdd = [ sym('th1dd');0;0;0;sym('th5dd') ];

I = [...
    sym('Ix1') sym('Ix2') sym('Ix3') sym('Ix4') sym('Ix5'); ...
    sym('Iy1') sym('Iy2') sym('Iy3') sym('Iy4') sym('Iy5'); ...
    sym('Iz1') sym('Iz2') sym('Iz3') sym('Iz4') sym('Iz5')...
    ];

B = [sym('B1');sym('B2');sym('B3');sym('B4');sym('B5')];
tau = [sym('tau1');sym('tau2');sym('tau3');sym('tau4');sym('tau5')];
taud = [sym('tau1d');sym('tau2d');sym('tau3d');sym('tau4d');sym('tau5d')];
m = [sym('m1');sym('m2');sym('m3');sym('m4');sym('m5')];

con_vals = sym(zeros(78,2));
setgb = 1;
end



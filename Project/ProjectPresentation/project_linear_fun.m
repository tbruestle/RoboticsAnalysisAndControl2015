% project_linear_fun.m
%
% ode45 function for project_linear 8
%
% x = [theta; d; theta_dot; d_dot];
function xder = project_linear_fun(t,x)
global Amat Bmat Nbar r kG
xder = (Amat - Bmat*kG)*x;% + Bmat*Nbar*r;
end


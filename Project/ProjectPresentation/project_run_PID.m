function [ t, x, tau_out ] = project_run_PID( )
global linkParams linkParamsC TSolve
global z0 q qd qdd qei
global qdes qddes qdddes
global qstart qdstart qddstart qeistart
global th thd thdd
global I tspan x0
global B tau taud ode45_eq ode45_u
global KP KI KD con_vals

syms  th1des d2des d3des d4des th5des
syms  th1ddes d2ddes d3ddes d4ddes th5ddes
syms  th1dddes d2dddes d3dddes d4dddes th5dddes

syms  th1start d2start d3start d4start th5start
syms  th1dstart d2dstart d3dstart d4dstart th5dstart
syms  th1ddstart d2ddstart d3ddstart d4ddstart th5ddstart
syms  th1eistart d2eistart d3eistart d4eistart th5eistart

syms  g
syms  th1 d2 d3 d4 th5
syms  th1d d2d d3d d4d th5d
syms  th1dd d2dd d3dd d4dd th5dd
syms  th1ei d2ei d3ei d4ei th5ei
syms  m1 m2 m3 m4 m5
syms  Ix1 Ix2 Ix3 Ix4 Ix5
syms  Iy1 Iy2 Iy3 Iy4 Iy5
syms  Iz1 Iz2 Iz3 Iz4 Iz5
syms  nx1 ny1 nz1
syms  ox1 oy1 oz1
syms  ax1 ay1 az1
syms  px1 py1 pz1
syms  B1 B2 B3 B4 B5
syms  tau1 tau2 tau3 tau4 tau5
syms  tau1d tau2d tau3d tau4d tau5d
syms  KP1 KP2 KP3 KP4 KP5
syms  KI1 KI2 KI3 KI4 KI5
syms  KD1 KD2 KD3 KD4 KD5
syms  l4 l5
%% First run with tau being derived from a PID controller
%% some of the controls use d tau/dt, but for those we still
%% use a PID controller instead of the derivative of a PID
%% controller as it is more versatile
xder1 = ode45_eq(1:15,3);
xder1 = project_sub_constants(xder1)
for i = 1:5
    xder1 = subs(xder1, tau(i), -KP(i)*(q(i)-qdes(i)) - KD(i)*qd(i) - KI(i)*qei(i));
    xder1 = subs(xder1, taud(i), -KP(i)*(q(i)-qdes(i)) - KD(i)*qd(i) - KI(i)*qei(i));
end
% In case there are any qdd terms left, fix the equation
% for i = 1:5
%     xder1(i+5) = (xder1(i+5) - qdd(i)*diff(xder1(i+5),qdd(i)))/(1-diff(xder1(i+5),qdd(i)));
% end

%Substitute in the constants with the previously derived values
xder1 = project_sub_constants(xder1);

xder1
%run the ode45 equation
ode45_eq(1:15,5) = xder1;
[t, x] = ode45('project_fun_PID',tspan,x0);

%derive the tau and d tau/dt values
for i=1:5
    qdesi = qdes(i);
    tau_out(:,i) = project_sub_constants(-KP(i).*(x(:,i)-qdesi) - KD(i).*x(:,i+5) - KI(i).*x(:,i+10));
end
end


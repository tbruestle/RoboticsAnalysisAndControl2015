function [ Aout ] = project_sub_constants( Ain )
global con_vals
Aout = Ain;
cnt = size(con_vals,1);
for i=1:cnt
    Aout = subs(Aout, con_vals(i,1), con_vals(i,2));
end

end


% project.m
%
% The main function
%
clear all; clc; clear figure;
%HW7_SETGLOBALS Sets globals for HW7
global linkParams linkParamsC TSolve
global z0 q qd qdd qei
global qdes qddes qdddes
global qstart qdstart qddstart qeistart
global th thd thdd
global I tspan x0
global B tau taud ode45_eq ode45_u
global KP KI KD con_vals
global m
global Xd Xdd

syms  th1des d2des d3des d4des th5des
syms  th1ddes d2ddes d3ddes d4ddes th5ddes
syms  th1dddes d2dddes d3dddes d4dddes th5dddes

syms  th1start d2start d3start d4start th5start
syms  th1dstart d2dstart d3dstart d4dstart th5dstart
syms  th1ddstart d2ddstart d3ddstart d4ddstart th5ddstart
syms  th1eistart d2eistart d3eistart d4eistart th5eistart

syms  g
syms  th1 d2 d3 d4 th5
syms  th1d d2d d3d d4d th5d
syms  th1dd d2dd d3dd d4dd th5dd
syms  th1ei d2ei d3ei d4ei th5ei
syms  m1 m2 m3 m4 m5
syms  Ix1 Ix2 Ix3 Ix4 Ix5
syms  Iy1 Iy2 Iy3 Iy4 Iy5
syms  Iz1 Iz2 Iz3 Iz4 Iz5
syms  nx1 ny1 nz1
syms  ox1 oy1 oz1
syms  ax1 ay1 az1
syms  px1 py1 pz1
syms  B1 B2 B3 B4 B5
syms  tau1 tau2 tau3 tau4 tau5
syms  tau1d tau2d tau3d tau4d tau5d
syms  KP1 KP2 KP3 KP4 KP5
syms  KI1 KI2 KI3 KI4 KI5
syms  KD1 KD2 KD3 KD4 KD5
syms  l4 l5
syms  th1err d2err d3err d4err th5err
syms  Xv1d Xv2d Xv3d 
syms  Xw1d Xw2d Xw3d
syms  Xv1dd Xv2dd Xv3dd 
syms  Xw1dd Xw2dd Xw3dd 
%% First I set up the global parameters
project_setGlobals;

%Using the DH Matrix values for the translations and the center of mass,
%derive the symbolic equations
T01 = DHmat( linkParams(1,1), linkParams(1,2), linkParams(1,3), linkParams(1,4) )
T12 = DHmat( linkParams(2,1), linkParams(2,2), linkParams(2,3), linkParams(2,4) )
T23 = DHmat( linkParams(3,1), linkParams(3,2), linkParams(3,3), linkParams(3,4) )
T34 = DHmat( linkParams(4,1), linkParams(4,2), linkParams(4,3), linkParams(4,4) )
T45 = DHmat( linkParams(5,1), linkParams(5,2), linkParams(5,3), linkParams(5,4) )

T01c = DHmat( linkParamsC(1,1), linkParamsC(1,2), linkParamsC(1,3), linkParamsC(1,4) )
T12c = DHmat( linkParamsC(2,1), linkParamsC(2,2), linkParamsC(2,3), linkParamsC(2,4) )
T23c = DHmat( linkParamsC(3,1), linkParamsC(3,2), linkParamsC(3,3), linkParamsC(3,4) )
T34c = DHmat( linkParamsC(4,1), linkParamsC(4,2), linkParamsC(4,3), linkParamsC(4,4) )
T45c = DHmat( linkParamsC(5,1), linkParamsC(5,2), linkParamsC(5,3), linkParamsC(5,4) )

T02 = T01*T12
T03 = T02*T23
T04 = T03*T34
T05 = T04*T45

T02c = T01*T12c
T03c = T02*T23c
T04c = T03*T34c
T05c = T04*T45c

%Solve the inverse kinematics equation. This equation uses the standard
%variables and finds several functions that find the valus for theta1, d2,
%d3, d4, and theta5 based on values of nx1, ny1, nz1, ox1, oy1, oz1, ax1,
%ay1, az1, px1, py1, pz1
T = T01*T12*T23*T34*T45
TSolve
inverse_kinematics = project_inverse_kinematics(T, TSolve)

%Find the translation vector as part of the transformation matrix
d01 = T01(1:3,4);
d02 = T02(1:3,4);
d03 = T03(1:3,4);
d04 = T04(1:3,4);
d05 = T05(1:3,4);

%Find the z rotation vector as part of the transformation matrix
z01 = T01(1:3,3);
z02 = T02(1:3,3);
z03 = T03(1:3,3);
z04 = T04(1:3,3);
z05 = T05(1:3,3);

%Find the J matrices
%These are unused in the application
for i=1:5
   Jv5 (:,i) =  diff(d05,q(i));
end
Jw5 = [ z0 0*z01 0*z02 0*z03 z04 ];
J5 = [ Jv5; Jw5 ]

for i=1:4
   Jv4 (:,i) =  diff(d04,q(i));
end
Jw4 = [ z0 0*z01 0*z02 0*z03 ];
J4 = [ Jv4; Jw4 ]

for i=1:3
   Jv3 (:,i) =  diff(d03,q(i));
end
Jw3 = [ z0 0*z01 0*z02 ];
J3 = [ Jv3; Jw3 ]

for i=1:2
   Jv2 (:,i) =  diff(d02,q(i));
end
Jw2 = [ z0 0*z01 ];
J2 = [ Jv2; Jw2 ]

for i=1:1
   Jv1 (:,i) =  diff(d01,q(i));
end
Jw1 = [ z0 ];
J1 = [ Jv1; Jw1 ]

%Find the dJ/dt matrices
Jstar5 = simplify((transpose(J5)*J5)^-1*transpose(J5))
Jstar4 = simplify((transpose(J4)*J4)^-1*transpose(J4))
Jstar3 = simplify((transpose(J3)*J3)^-1*transpose(J3))
Jstar2 = simplify((transpose(J2)*J2)^-1*transpose(J2))
Jstar1 = simplify((transpose(J1)*J1)^-1*transpose(J1))

%Find the J_dot matrices
J5_dot = simplify(project_time_derivative(J5))
J4_dot = simplify(project_time_derivative(J4))
J3_dot = simplify(project_time_derivative(J3))
J2_dot = simplify(project_time_derivative(J2))
J1_dot = simplify(project_time_derivative(J1))

%Find the J_dot_dot matrices
J5_dot_dot = simplify(project_time_derivative(project_time_derivative(J5)))
J4_dot_dot = simplify(project_time_derivative(project_time_derivative(J4)))
J3_dot_dot = simplify(project_time_derivative(project_time_derivative(J3)))
J2_dot_dot = simplify(project_time_derivative(project_time_derivative(J2)))
J1_dot_dot = simplify(project_time_derivative(project_time_derivative(J1)))

qdSolve = Jstar5*Xd
qddSolve = Jstar5*(Xdd - J5_dot*qd)
XdSolve = J5*qd
XddSolve = J5*qdd + J5_dot*qd
%Find the angular velocity vector to be used in the energy equation
%These are based on the center of mass
wp1 = z0*thd(1);
wp2 = transpose(T01(1:3,1:3))*(wp1+z0*thd(2));
wp3 = transpose(T02(1:3,1:3))*(wp2+z0*thd(3));
wp4 = transpose(T03(1:3,1:3))*(wp3+z0*thd(4));
wp5 = transpose(T04(1:3,1:3))*(wp4+z0*thd(5));
wp1
wp2
wp3
wp4
wp5

%Find the translational velocity to be used in the energy equation
rd01c = [0; 0; 0];
rd02c = [0; 0; 0];
rd03c = [0; 0; 0];
rd04c = [0; 0; 0];
rd05c = [0; 0; 0];
for j=1:i
    rd01c = rd01c + diff(T01c(1:3,4),q(j))*qd(j);
    rd02c = rd02c + diff(T02c(1:3,4),q(j))*qd(j);
    rd03c = rd03c + diff(T03c(1:3,4),q(j))*qd(j);
    rd04c = rd04c + diff(T04c(1:3,4),q(j))*qd(j);
    rd05c = rd05c + diff(T05c(1:3,4),q(j))*qd(j);
end
rd01c
rd02c
rd03c
rd04c
rd05c

%Find the potential energy due to gravity
P1 = sym('m1')*[0 0 sym('g')]*T01c(1:3,4);
P2 = sym('m2')*[0 0 sym('g')]*T02c(1:3,4);
P3 = sym('m3')*[0 0 sym('g')]*T03c(1:3,4);
P4 = sym('m4')*[0 0 sym('g')]*T04c(1:3,4);
P5 = sym('m5')*[0 0 sym('g')]*T05c(1:3,4);
P1
P2
P3
P4
P5
P = P1 + P2 + P3 + P4 + P5

%Using the rotational 
%Since the robot is rotating, not all transformations have a mass
%associated with the transformation, and thus the m values are 0
K1 = 0;
K2 = 0;
K3 = 0;
K4 = 0;
K5 = 0;
for j=1:3
    K1 = K1 + 1/2*sym('m1')*rd01c(j)^2 + 1/2*I(j,1)*wp1(j)^2;
    K2 = K2 + 1/2*sym('m2')*rd02c(j)^2 + 1/2*I(j,2)*wp2(j)^2;
    K3 = K3 + 1/2*sym('m3')*rd03c(j)^2 + 1/2*I(j,3)*wp3(j)^2;
    K4 = K4 + 1/2*sym('m4')*rd04c(j)^2 + 1/2*I(j,4)*wp4(j)^2;
    K5 = K5 + 1/2*sym('m5')*rd05c(j)^2 + 1/2*I(j,5)*wp5(j)^2;
end
K1
K2
K3
K4
K5
K = simplify(K1 + K2 + K3 + K4 + K5)

%Find the Lagrangian from the Kinetic and Potential energy
L = K - P


%Using our standard equations of motion incorporating the Lagrangian,
%We can derive 5 different equalities that equal zero.
%We can use these inequalities later to find definitive equations for
%the second order terms
for i=1:5
    eq(i,1) = project_time_derivative(diff(L,qd(i)))-diff(L,q(i)) + B(i)*qd(i) - tau(i);
end
eq = simplify(eq)

gv = sym(zeros(5,1));
cv = sym(zeros(5,1));
D = sym(zeros(5,5));

%Derive the Christoff symbols based on the method in the book
ChrD = sym(zeros(5,5));
ChrC = sym(zeros(5,5,5));
ChrG = sym(zeros(5,1));

for k=1:5
    ChrG(k,1) = diff(P,q(k));
end
for j=1:5
    for k=1:5
        ChrD(j,k) = diff(diff(K,qd(k)),qd(j));
    end
end
for k=1:5
    for i=1:5
        for j=1:5
            ChrC(i,j,k) = 0.5*( ...
                diff(ChrD(k,j),q(i)) ...
                + diff(ChrD(k,i),q(j)) ...
                - diff(ChrD(i,j),q(k)) ...
            );
        end
    end
end

%Derive Christoff-like symbols taken from the Scara Manipulator file
for i=1:5
    gv(i,1) = diff(P,q(i));
    cv(i,1) = -diff(K,q(i));
    dKdqd = diff(K,qd(i));
    for j=1:5
        D(i,j) = diff(dKdqd,qd(j));
        cv(i,1) = cv(i,1) + diff(dKdqd,q(j))*qd(j);
    end
end
D = simplify(D)
gv = simplify(gv)
cv = simplify(cv)

%Since not all the equations have second order terms
%and not all second order terms are represented
%I wrote a custom solver that derives 5 appropriate functions
project_setup_ode45(L);
ode45_eq

x0sym = ode45_eq(1:15,4);

%% Derive the desired matrix from the points using these equations
%% These are the equations found when doing inverse kinematics
%% As the equations are complicated enough, I have chosen
%% friendly values
Tdes1 = [...
    1   0   0   1;...
    0   0   1   1;...
    0   1   0   1;...
    0   0   0   1;...
    ];
d4des1 = Tdes1(3,4);
d3des1 = Tdes1(2,3)*Tdes1(1,4) - Tdes1(1,3)*Tdes1(2,4);
d2des1 = Tdes1(1,3)*Tdes1(1,4) + Tdes1(2,3)*Tdes1(2,4);
if Tdes1(1,3) > 0
    th1des1 = acos(-Tdes1(2,3));
else
    th1des1 = 2*sym('pi') - acos(-Tdes1(2,3));
end
if Tdes1(3,1) < 0
    th5des1 = acos(-Tdes1(3,2));
else
    th5des1 = 2*sym('pi') - acos(-Tdes1(3,2));
end
qdes1 = [th1des1;d2des1;d3des1;d4des1;th5des1] %%[pi;1;1;1;pi]
% [             d4,               pz1]
% [              0,               az1]
% [    d2^2 + d3^2,     px1^2 + py1^2]
% [             d3, ay1*px1 - ax1*py1]
% [             d2, ax1*px1 + ay1*py1]
% [       sin(th1),               ax1]
% [       cos(th1),              -ay1]
% [ asin(sin(th1)),         asin(ax1)]
% [ acos(cos(th1)),    pi - acos(ay1)]
% [       sin(th5),              -nz1]
% [       cos(th5),              -oz1]
% [ asin(sin(th5)),        -asin(nz1)]
% [ acos(cos(th5)),    pi - acos(oz1)]

project_set_constants('qdes',qdes1);

%%All the initial values are zero and the robot is desired to end at rest
project_set_constants('qddes',[0;0;0;0;0]);
project_set_constants('qdddes',[0;0;0;0;0]);
project_set_constants('qeistart',[0;0;0;0;0]);
project_set_constants('qstart',[0;0;0;0;0]);
project_set_constants('qdstart',[0;0;0;0;0]);
project_set_constants('qddstart',[0;0;0;0;0]);
%Since the robot is rotating, not all transformations have a mass
%associated with the transformation, and thus the I values are 0
%The fourth mass is the mass of the robot
%The fifth mass is the mass of the elevator
project_set_constants('I',[...
        0 0 0 10 2; ...
        0 0 0 10 2; ...
        0 0 0 10 2 ...
    ]);
%Since the robot is rotating, not all transformations have a mass
%associated with the transformation, and thus the m values are 0
%The fourth mass is the mass of the robot
%The fifth mass is the mass of the elevator
project_set_constants('m',[0;0;0;100;20]);

%I left all B values as 1 as that seems the default
project_set_constants('B',[1;1;1;1;1]);

%KP values were tweaked to stabilize
project_set_constants('KP',[50;2;2;10;10]);
project_set_constants('KI',[2;5;5;0;0]);
project_set_constants('KD',[0;250;250;1;5]);

%l4 is the distance above the center of the robot that the robot's mass is
%located
%Since the elevator is above the center of the robot, this extra weight
%represents the weight of the elevator making the center of mass higher
project_set_constants('l4',[0.1]);

%l4 is the distance from the center of the robot that the elevator's mass is
%located
%The elevator is in the front of the robot, but most of the parts of the
%elevator are near the center of mass
project_set_constants('l5',[0.1]);

%gravity = 9.81, this means everything above is being measured in meters
%and seconds
project_set_constants('g',[9.81]);
con_vals

%we have devined the constants for x0, but now that we have values we can
%substitute
x0 = double(project_sub_constants(x0sym))

tspan = [0 1];
xeq = ode45_eq(1:15,3);
xeq = project_sub_constants(xeq)

project_linearize();

[ t, x, tau_out ] = project_run_PID( );

figure(1);
plot(t,x(:,1),t,x(:,2),t,x(:,3),t,x(:,4),t,x(:,5));
xlabel('time(s)');
ylabel('states');
legend('\theta1','d2','d3','d4','\theta5');

figure(2);
plot( t,tau_out(:,1),t,tau_out(:,2),t,tau_out(:,3),t,tau_out(:,4),t,tau_out(:,5) );
xlabel('time(s)');
ylabel('control input');
legend('\tau1','d\tau2/dt','d\tau3/dt','d\tau4/dt','\tau5');



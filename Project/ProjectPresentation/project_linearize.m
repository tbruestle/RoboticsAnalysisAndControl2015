function [ A1, B1, C1, D1  ] = project_linearize(  )

global linkParams linkParamsC TSolve
global z0 q qd qdd qei
global qdes qddes qdddes
global qstart qdstart qddstart qeistart
global th thd thdd
global I tspan x0
global B tau taud ode45_eq ode45_u
global KP KI KD con_vals
global Amat Bmat Nbar kG r

syms  th1des d2des d3des d4des th5des
syms  th1ddes d2ddes d3ddes d4ddes th5ddes
syms  th1dddes d2dddes d3dddes d4dddes th5dddes

syms  th1start d2start d3start d4start th5start
syms  th1dstart d2dstart d3dstart d4dstart th5dstart
syms  th1ddstart d2ddstart d3ddstart d4ddstart th5ddstart
syms  th1eistart d2eistart d3eistart d4eistart th5eistart

syms  g
syms  th1 d2 d3 d4 th5
syms  th1d d2d d3d d4d th5d
syms  th1dd d2dd d3dd d4dd th5dd
syms  th1ei d2ei d3ei d4ei th5ei
syms  m1 m2 m3 m4 m5
syms  Ix1 Ix2 Ix3 Ix4 Ix5
syms  Iy1 Iy2 Iy3 Iy4 Iy5
syms  Iz1 Iz2 Iz3 Iz4 Iz5
syms  nx1 ny1 nz1
syms  ox1 oy1 oz1
syms  ax1 ay1 az1
syms  px1 py1 pz1
syms  B1 B2 B3 B4 B5
syms  tau1 tau2 tau3 tau4 tau5
syms  tau1d tau2d tau3d tau4d tau5d
syms  KP1 KP2 KP3 KP4 KP5
syms  KI1 KI2 KI3 KI4 KI5
syms  KD1 KD2 KD3 KD4 KD5
syms  l4 l5
syms  th1err d2err d3err d4err th5err
xeq = ode45_eq(:,3);
xeq = project_sub_constants(xeq);


sublist = [...
    q   qdes;
    qd   qddes;
    qei   zeros(5,1);
    ];
Amat = sym(zeros(size(xeq,1),15));
Bmat = sym(zeros(size(xeq,1),5));
for i = 1:15
    Amat(:,i) = diff(xeq,sublist(i,1));
end
for i = 1:5
    Bmat(:,i) = diff(xeq,tau(i,1)) + diff(xeq,taud(i,1));
end
for i = 1:15
    Amat = subs(Amat,sublist(i,1),sublist(i,2));
    Bmat = subs(Bmat,sublist(i,1),sublist(i,2));
end
for i = 1:5
    Amat = subs(Amat,tau(i,1),0);
    Bmat = subs(Bmat,tau(i,1),0);
    Amat = subs(Amat,taud(i,1),0);
    Bmat = subs(Bmat,taud(i,1),0);
end
Amat
Bmat
Amat = double(project_sub_constants(Amat));
Bmat = double(project_sub_constants(Bmat));
Cmat = double([ eye(5) zeros(5,10) ]);
Dmat = double([ zeros(5,5) ]);

Amat
Bmat
Cmat
Dmat

Cm = [Bmat Amat*Bmat Amat*Amat*Bmat]
Om = [Cmat;Cmat*Amat;Cmat*Amat*Amat]
Cmdet = det([Bmat Amat*Bmat Amat*Amat*Bmat])
Omdet = det([Cmat;Cmat*Amat;Cmat*Amat*Amat])


%lamO = eig(Amat)
%kG = place(A,B,lamC)
%kO = place(Amat,Bmat,lamO)
tfin = 8;
tspan = [0 tfin];

for settletime_des = [1:3:4]
    settletime = zeros(1,2);
    minQ = 0;
    maxQ = 10000;
    threshold = 0.05;
    x0
    while maxQ - minQ > 0.000001
        currQ = (minQ + maxQ)/2;
        Q = diag([currQ currQ currQ currQ currQ 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1]);
        R = diag([.1 .1 .1 .1 .1]);
        kG = lqr(Amat,Bmat,Q,R)
        %Nbar = rscalen(Amat,Bmat,Cmat,Dmat,kG);
        r = [1;1;1;1;1];
        
        %Amat*x0
        %Bmat*kG*x0
        %Bmat*Nbar*r
        %fun_out = Amat - Bmat*kG
        [t,x] = ode45('project_linear_fun',tspan,-double(project_sub_constants([qdes;qddes;qdddes])));


        cntT = size(x,1);

        for i=1:cntT-1
            if abs(x(i,1)-r(1)) > threshold
                %settle1 = i;
                settletime(1) = t(i+1);
            end
            if abs(x(i,2)-r(2)) > threshold
                %settle2 = i;
                settletime(2) = t(i+1);
            end
        end
        [max(settletime) currQ minQ maxQ];
        if max(settletime) > settletime_des
            minQ = currQ;
        else
            maxQ = currQ;
        end
    end
    settletime
    settletime_des
    kG
    Q
    R
    t;
    x;
    Bmat*kG;
    u = transpose(Bmat*kG*transpose(x));
    
    if settletime_des == 1
        figure(3);
    else
        figure(5);
    end  
    
    qdes1 = project_sub_constants(qdes);
    for i = 1:5
        cnt = size(x,1);
        for j = 1:cnt
            x(j,i) = x(j,i)+qdes1(i);
        end
    end
    
    plot(t,x(:,1),t,x(:,2),t,x(:,3),t,x(:,4),t,x(:,5));
    xlabel('time(s)');
    ylabel('states');
    legend('\theta1','d2','d3','d4','\theta5');
    
    
    if settletime_des == 1
        figure(4);
    else
        figure(6);
    end  
    
    plot(t,u(:,6),t,u(:,7),t,u(:,8),t,u(:,9),t,u(:,10));
    xlabel('time(s)');
    ylabel('states');
    legend('Tau 1','Tau 2','Tau 3','Tau 4','Tau 5');
end
% 
% xeq = subs(xeq,...
%     (4*d2 + 2*cos(th1)^2 + 120*d2^2 + 120*d3^2 + 51/5),...
%     (4*d2des + 2*cos(th1des)^2 + 120*d2des^2 + 120*d3des^2 + 51/5)...
%     );

% 
% xeq = subs(xeq,th1,th1err + th1des);
% xeq = subs(xeq,d2,d2err + d2des);
% xeq = subs(xeq,d3,d3err + d3des);
% xeq = subs(xeq,d4,d4err + d4des);
% xeq = subs(xeq,th5,th5err + th5des);
% 
% xeq = project_sub_constants(xeq);

% xeq
end


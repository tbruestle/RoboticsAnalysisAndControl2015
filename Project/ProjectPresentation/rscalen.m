function Nbar = rscalen(A,B,C,D,K)
% Given the multi-input linear system: 
%       .
%       x = Ax + Bu
%       y = Cx + Du
% and the feedback matrix K,
% 
% the function rscale(A,B,C,D,K) finds the scale factor N which will 
% eliminate the steady-state error to a reference step R
% using the schematic below:
%
%                         /---------\
%      R         +     u  | .       |
%      ---> N --->() ---->| X=Ax+Bu |--> y=Cx ---> y
%                -|       \---------/
%                 |             | 
%                 |<---- K <----|
%
%8/21/96 Yanjie Sun of the University of Michigan
%        under the supervision of Prof. D. Tilbury
%3/15/01 Modified by Dr. H. Ashrafiuon of Villanova University
%
s = size(A,1);
su = size(C,1);
Z = [zeros([su,s]) eye(su)];
N = [A,B;C,D]\(Z');
Nx = N(1:s,1:su);
Nu(1:su,1:su) = N(s+1:s+su,1:su);
Nbar=Nu + K*Nx;
